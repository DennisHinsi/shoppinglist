/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dennis.hinsi.domain.model.Good

@Dao
interface GoodDAO {
    @Insert
    suspend fun insert(good: Good)

    @Update
    suspend fun update(good: Good)

    @Query("DELETE FROM good_table WHERE id = :goodId")
    suspend fun delete(goodId: String)

    @Query("SELECT * FROM good_table WHERE id = :id")
    suspend fun load(id: String): Good

    @Query("SELECT * FROM good_table WHERE group_id = :groupId")
    suspend fun loadGroup(groupId: String): List<Good>

    @Query("SELECT * FROM good_table WHERE group_id = :groupId")
    fun observeGroup(groupId: String): LiveData<List<Good>>
}