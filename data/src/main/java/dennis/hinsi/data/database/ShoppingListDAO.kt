/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dennis.hinsi.domain.model.ShoppingList

@Dao
interface ShoppingListDAO {
    @Insert
    suspend fun insert(shoppingList: ShoppingList)

    @Update
    suspend fun update(shoppingList: ShoppingList)

    @Query("DELETE FROM shopping_list WHERE id = :id")
    suspend fun delete(id: String)

    @Query("SELECT * FROM shopping_list WHERE id = :id")
    fun load(id: String): LiveData<ShoppingList>

    @Query("SELECT * FROM shopping_list WHERE id = :id")
    fun loadSingle(id: String): ShoppingList

    @Query("SELECT * FROM shopping_list")
    fun observeLists(): LiveData<List<ShoppingList>>
}