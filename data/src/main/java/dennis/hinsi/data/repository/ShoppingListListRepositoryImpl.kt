/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.data.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.data.database.ShoppingListDAO
import dennis.hinsi.domain.model.ShoppingList
import dennis.hinsi.domain.repository.ShoppingListRepository

class ShoppingListListRepositoryImpl(
    private val shoppingListDAO: ShoppingListDAO
) : ShoppingListRepository {
    override suspend fun insert(shoppingList: ShoppingList) = shoppingListDAO.insert(shoppingList)

    override suspend fun update(shoppingList: ShoppingList) = shoppingListDAO.update(shoppingList)

    override suspend fun delete(id: String) = shoppingListDAO.delete(id)

    override fun load(id: String): LiveData<ShoppingList> = shoppingListDAO.load(id)

    override suspend fun loadSingle(id: String): ShoppingList = shoppingListDAO.loadSingle(id)

    override fun observeLists(): LiveData<List<ShoppingList>> = shoppingListDAO.observeLists()
}