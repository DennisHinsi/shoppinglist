<h1>ShoppingList</h1>

A simple app to manage shopping lists. It consists of a overview page and a detail page with all goods needed. 

This project is modular structured following the rules of **Clean Architecture**.

Note: A good guide to Clean Architecture: https://medium.com/@pxpgraphics/clean-architecture-3fe6907e7441

    There are three modules:
    -  app
    -  data
    -  domain

<h2>Supported Android versions:</h2>

- Android 6.0+

Tested on devices with:

-   Android 8.0
-   Android 10

<h2>License</h2>
	
    Copyright 2020 dennis hinsi

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 	