/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.repository

import androidx.lifecycle.LiveData
import dennis.hinsi.domain.model.Good

interface GoodRepository {
    suspend fun insert(good: Good)
    suspend fun update(good: Good)
    suspend fun delete(goodId: String)
    suspend fun load(id: String): Good
    suspend fun loadGroup(groupId: String): List<Good>
    fun observeGroup(groupId: String): LiveData<List<Good>>
}