/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecases

import dagger.Module
import dennis.hinsi.domain.model.Good
import dennis.hinsi.domain.repository.GoodRepository

@Module
class GoodWriterUC(
    private val goodRepository: GoodRepository,
    private val shoppingListWriterUC: ShoppingListWriterUC
) {

    suspend fun insert(good: Good) {
        goodRepository.insert(good)
        shoppingListWriterUC.updateTimestamp(good.groupId)
    }

    suspend fun update(good: Good) {
        goodRepository.update(good)
        shoppingListWriterUC.updateTimestamp(good.groupId)
    }

    suspend fun delete(goodId: String) {
        goodRepository.delete(goodId)
        shoppingListWriterUC.updateTimestamp(goodId)
    }
}