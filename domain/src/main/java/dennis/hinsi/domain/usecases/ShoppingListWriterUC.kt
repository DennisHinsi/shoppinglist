/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecases

import dagger.Module
import dennis.hinsi.domain.model.ShoppingList
import dennis.hinsi.domain.repository.ShoppingListRepository

@Module
class ShoppingListWriterUC(private val shoppingListRepository: ShoppingListRepository) {

    suspend fun insert(shoppingList: ShoppingList) =
        shoppingListRepository.insert(shoppingList.apply {
            modified = System.currentTimeMillis()
        })

    suspend fun update(shoppingList: ShoppingList) =
        shoppingListRepository.update(shoppingList.apply {
            modified = System.currentTimeMillis()
        })

    suspend fun delete(id: String) = shoppingListRepository.delete(id)

    suspend fun updateTimestamp(id: String) {
        shoppingListRepository.loadSingle(id).apply {
            modified = System.currentTimeMillis()
        }.also { shoppingList ->
            shoppingListRepository.update(shoppingList)
        }
    }
}