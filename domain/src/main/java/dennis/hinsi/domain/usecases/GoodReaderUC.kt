/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.domain.usecases

import androidx.lifecycle.LiveData
import dagger.Module
import dennis.hinsi.domain.model.Good
import dennis.hinsi.domain.repository.GoodRepository

@Module
class GoodReaderUC (private val goodRepository: GoodRepository) {

    suspend fun load(id: String): Good = goodRepository.load(id)

    suspend fun loadGroup(groupId: String): List<Good> = goodRepository.loadGroup(groupId)

    fun observeGroup(groupId: String): LiveData<List<Good>> = goodRepository.observeGroup(groupId)

}