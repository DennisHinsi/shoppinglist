/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import dennis.hinsi.domain.model.GoodType
import dennis.hinsi.shoppinglist.ui.custom.SelectionPair

infix fun <K, V> K.toSelectionPair(v: V) = SelectionPair(this, v)

fun ExtendedFloatingActionButton.setupWithRecyclerView(recyclerView: RecyclerView) {
    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)

            if (!recyclerView.canScrollVertically(1) &&
                recyclerView.computeVerticalScrollRange() >= recyclerView.getHeight()
            ) {
                this@setupWithRecyclerView.shrink()
            } else {
                if (!this@setupWithRecyclerView.isExtended)
                    this@setupWithRecyclerView.extend()
            }
        }
    })
}

fun <T> T?.requireNonNull(): T =
    if (this != null)
        this
    else
        throw NullPointerException()

fun TextView.setGoodTypeText(goodType: GoodType) {
    text = when (goodType) {
        GoodType.COMMON -> "\uD83D\uDED2"
        GoodType.MEAT -> "\uD83E\uDD69"
        GoodType.MILK -> "\uD83D\uDC04"
        GoodType.GRAIN -> "\uD83C\uDF3E"
        GoodType.FAT -> "\uD83E\uDDC8"
        GoodType.SWEET -> "\uD83C\uDF6C"
        GoodType.DRINK -> "\uD83E\uDD64"
        GoodType.TECHNOLOGY -> "\uD83D\uDCBB"
        GoodType.GIFT -> "\uD83C\uDF81"
        GoodType.BOOK -> "\uD83D\uDCD2"
        GoodType.WORKBENCH -> "\uD83D\uDEE0️"
        GoodType.HEALTH -> "\uD83E\uDE79"
        GoodType.HYGIENE -> "\uD83E\uDDFC"
        GoodType.ANIMALS -> "\uD83D\uDC08"
    }
}
