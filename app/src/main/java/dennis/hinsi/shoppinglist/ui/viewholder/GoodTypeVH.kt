/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.domain.model.GoodType
import dennis.hinsi.shoppinglist.ui.custom.SelectionPair
import dennis.hinsi.shoppinglist.ui.setGoodTypeText

class GoodTypeVH(private val view: View) : RecyclerView.ViewHolder(view) {

    private val itemIcon = view.findViewById(R.id.item_icon) as TextView

    fun bind(
        item: SelectionPair<GoodType, Boolean>,
        click: (GoodType) -> Unit
    ) {
        view.setOnClickListener {
            click(item.first)
        }

        if (item.second)
            itemIcon.background = ResourcesCompat.getDrawable(
                view.resources,
                R.drawable.bg_selection,
                view.context.theme
            )
        else
            itemIcon.background = null

        itemIcon.setGoodTypeText(item.first)
    }

    companion object {
        fun newInstance(parent: ViewGroup): GoodTypeVH =
            GoodTypeVH(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_good_type, parent, false)
            )
    }
}
