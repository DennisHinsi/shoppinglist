/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.base.BaseVH
import dennis.hinsi.domain.model.Good
import dennis.hinsi.shoppinglist.ui.custom.ClickObject
import dennis.hinsi.shoppinglist.ui.setGoodTypeText
import io.reactivex.rxjava3.subjects.PublishSubject

class GoodItemVH(private val view: View) : BaseVH<Good, String>(view) {

    private val itemIcon = view.findViewById(R.id.item_icon) as TextView
    private val itemText = view.findViewById(R.id.item_text) as TextView
    private val itemNumber = view.findViewById(R.id.item_number) as TextView
    private val itemDivider = view.findViewById(R.id.item_divider) as View
    private val itemSelectionIndicator = view.findViewById(R.id.item_selection_indicator) as View

    var isDone: Boolean = false
        private set

    override fun bind(
        item: Good,
        isLast: Boolean,
        clickStream: PublishSubject<ClickObject<String>>
    ) {
        view.setOnClickListener {
            clickStream.onNext(ClickObject(item.id))
        }

        view.setOnLongClickListener {
            clickStream.onNext(ClickObject(item.id, true))
            true
        }

        itemText.text = item.name
        itemIcon.setGoodTypeText(item.type)
        itemNumber.text = item.number.toString()
        isDone = item.isDone
        itemSelectionIndicator.visibility = if (item.isDone) View.VISIBLE else View.GONE
        itemDivider.visibility = if (isLast) View.INVISIBLE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup): GoodItemVH =
            GoodItemVH(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_good, parent, false)
            )
    }
}
