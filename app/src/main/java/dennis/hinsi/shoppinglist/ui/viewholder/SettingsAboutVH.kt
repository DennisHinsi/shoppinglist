/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.base.BaseVH
import dennis.hinsi.domain.model.SettingsAbout
import dennis.hinsi.shoppinglist.ui.custom.ClickObject
import io.reactivex.rxjava3.subjects.PublishSubject

class SettingsAboutVH(private val view: View) : BaseVH<SettingsAbout, String>(view) {

    private val itemTextKey = view.findViewById(R.id.item_text_key) as TextView
    private val itemTextVal = view.findViewById(R.id.item_text_value) as TextView
    private val itemDivider = view.findViewById(R.id.item_divider) as View

    override fun bind(
        item: SettingsAbout,
        isLast: Boolean,
        clickStream: PublishSubject<ClickObject<String>>
    ) {
        itemTextKey.text = item.titleKey
        itemTextVal.text = item.titleValue
        itemDivider.visibility = if (isLast) View.INVISIBLE else View.VISIBLE
    }

    companion object {
        fun newInstance(parent: ViewGroup): SettingsAboutVH =
            SettingsAboutVH(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_settings_about, parent, false)
            )
    }
}
