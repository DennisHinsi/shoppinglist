/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.callback

import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.shoppinglist.ui.adapter.GoodAdapter
import dennis.hinsi.shoppinglist.ui.viewholder.GoodItemVH

class GoodAdapterCallback(
    private val iconCheckTrue: Drawable,
    private val iconCheckFalse: Drawable,
    private val checkTrueBackground: ColorDrawable,
    private val checkFalseBackground: ColorDrawable,
    private val adapter: GoodAdapter,
    private val onSwiped: (String) -> Unit
) : ItemTouchHelper.SimpleCallback(
    0,
    ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
) {


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        onSwiped(adapter.list[position].id)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean = true

    override fun onChildDraw(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        state: Int,
        isActive: Boolean
    ) {
        val icon = if (!(viewHolder as GoodItemVH).isDone)
            iconCheckTrue else iconCheckFalse
        val background = if (!viewHolder.isDone)
            checkTrueBackground else checkFalseBackground

        val itemRootView = viewHolder.itemView
        when {
            dX < 0 -> {
                val iconMargin = (itemRootView.height - icon.intrinsicHeight) / 2
                val iconTop = itemRootView.top + iconMargin
                val iconBottom = iconTop + icon.intrinsicHeight
                val iconLeft = itemRootView.right - iconMargin - icon.intrinsicWidth
                val iconRight = itemRootView.right - iconMargin

                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.draw(canvas)
                icon.draw(canvas)

                val swipeDrawBorder = -(itemRootView.width - itemRootView.height * 2)
                val leftBorder = -(itemRootView.width + dX)
                if (leftBorder < swipeDrawBorder) {
                    background.setBounds(
                        itemRootView.right + dX.toInt(),
                        itemRootView.top, itemRootView.right, itemRootView.bottom
                    )
                    super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, state, isActive)
                }
            }
            dX > 0 -> {
                val iconMargin = (itemRootView.height - icon.intrinsicHeight) / 2
                val iconTop = itemRootView.top + iconMargin
                val iconBottom = iconTop + icon.intrinsicHeight
                val iconLeft = itemRootView.left + iconMargin
                val iconRight = itemRootView.left + iconMargin + icon.intrinsicWidth

                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.draw(canvas)
                icon.draw(canvas)

                val swipeDrawBorder = itemRootView.width + itemRootView.height * 2
                val leftBorder = itemRootView.width + dX
                if (leftBorder < swipeDrawBorder) {
                    background.setBounds(
                        itemRootView.left + dX.toInt(),
                        itemRootView.top, itemRootView.left, itemRootView.bottom
                    )
                    super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, state, isActive)
                }
            }
        }
    }
}