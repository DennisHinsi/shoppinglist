/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.domain.model.GoodType
import dennis.hinsi.shoppinglist.ui.custom.SelectionPair
import dennis.hinsi.shoppinglist.ui.viewholder.GoodTypeVH

class GoodTypeAdapter : RecyclerView.Adapter<GoodTypeVH>() {

    var selection: GoodType? = null
        private set

    private var list = mutableListOf<SelectionPair<GoodType, Boolean>>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GoodTypeVH = GoodTypeVH.newInstance(parent)
    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: GoodTypeVH, position: Int) {
        holder.bind(list[position]) { selectedType ->
            selection = selectedType
            select(selectedType)
        }
    }

    fun setList(list: MutableList<SelectionPair<GoodType, Boolean>>) {
        this.list = list
        if (list.isNotEmpty())
            selection = list[0].first
        notifyDataSetChanged()
    }

    fun select(item: GoodType) {
        selection = item
        list.apply {
            forEach { eachPair ->
                eachPair.second = eachPair.first == item
            }
        }
        notifyDataSetChanged()
    }
}