/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.fragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.base.BaseFragment
import dennis.hinsi.shoppinglist.ui.RxAttrs
import dennis.hinsi.shoppinglist.ui.adapter.GoodAdapter
import dennis.hinsi.shoppinglist.ui.callback.GoodAdapterCallback
import dennis.hinsi.shoppinglist.ui.custom.ClickObject
import dennis.hinsi.shoppinglist.ui.requireNonNull
import dennis.hinsi.shoppinglist.ui.setupWithRecyclerView
import dennis.hinsi.shoppinglist.ui.vm.ShoppingDetailVM
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.frg_shopping_detail.fab_add
import kotlinx.android.synthetic.main.frg_shopping_detail.recycler_view
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ShoppingDetailFragment : BaseFragment() {

    @Inject
    lateinit var shoppingDetailVM: ShoppingDetailVM

    private val args: ShoppingDetailFragmentArgs by navArgs()
    private lateinit var goodAdapter: GoodAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_shopping_detail, container, false)

    override fun prepareUI() {
        goodAdapter = GoodAdapter()
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        recycler_view.adapter = goodAdapter
        setupAdapterCallback()
        fab_add.setupWithRecyclerView(recycler_view)
        setHasOptionsMenu(true)
    }

    override fun prepareOnClickListeners() {
        goodAdapter.clickStream
            .doOnError(::streamError)
            .subscribe { adapterClickAction(it) }
            .addToSubscriptions()

        fab_add.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .doOnError(::streamError)
            .subscribe { fabClickAction() }
            .addToSubscriptions()
    }

    override fun observeData() {
        shoppingDetailVM.load(args.shoppingListId).observe(viewLifecycleOwner, Observer {
            requireActivity().toolbar.title = it.name
            Navigation.findNavController(requireView()).currentDestination?.label = it.name
        })

        shoppingDetailVM.observerGroup(args.shoppingListId).observe(viewLifecycleOwner, Observer {
            goodAdapter.list = it.sortedByDescending { !it.isDone }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_mark_as_done -> shoppingDetailVM.updateGroupDoneState(
                groupId = args.shoppingListId,
                newState = true
            )

            R.id.menu_mark_as_not_done -> shoppingDetailVM.updateGroupDoneState(
                groupId = args.shoppingListId,
                newState = false
            )
        }
        return true
    }

    private fun setupAdapterCallback() {
        ItemTouchHelper(
            GoodAdapterCallback(
                iconCheckTrue = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_check_true_24,
                    requireContext().theme
                ).requireNonNull(),

                iconCheckFalse = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_check_false_24,
                    requireContext().theme
                ).requireNonNull(),

                checkTrueBackground = ColorDrawable(Color.GREEN),

                checkFalseBackground = ColorDrawable(Color.RED),

                adapter = goodAdapter
            ) { id ->
                goodAdapter.invertSelectionOf(id)
                shoppingDetailVM.updateDoneState(id)
            }
        ).also { helper -> helper.attachToRecyclerView(recycler_view) }
    }

    private fun adapterClickAction(clickObject: ClickObject<String>) {
        when (clickObject.isLongClick) {
            true -> findNavController().navigate(
                ShoppingDetailFragmentDirections.actionShoppingDetailFragmentToDeleteGoodDialog(
                    goodId = clickObject.value
                )
            )
            false -> findNavController().navigate(
                ShoppingDetailFragmentDirections.actionShoppingDetailFragmentToGoodEditorDialog(
                    shoppingListId = args.shoppingListId,
                    goodId = clickObject.value
                )
            )
        }
    }

    private fun fabClickAction() {
        findNavController().navigate(
            ShoppingDetailFragmentDirections.actionShoppingDetailFragmentToGoodEditorDialog(
                shoppingListId = args.shoppingListId,
                goodId = null
            )
        )
    }
}