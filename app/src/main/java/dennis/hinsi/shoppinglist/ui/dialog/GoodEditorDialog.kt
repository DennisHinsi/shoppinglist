/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.domain.model.Good
import dennis.hinsi.domain.model.GoodType
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.base.BaseSheetDialogFragment
import dennis.hinsi.shoppinglist.ui.RxAttrs
import dennis.hinsi.shoppinglist.ui.adapter.GoodTypeAdapter
import dennis.hinsi.shoppinglist.ui.requireNonNull
import dennis.hinsi.shoppinglist.ui.toSelectionPair
import dennis.hinsi.shoppinglist.ui.vm.ShoppingDetailVM
import kotlinx.android.synthetic.main.dlg_good_editor.*
import kotlinx.android.synthetic.main.dlg_header.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class GoodEditorDialog : BaseSheetDialogFragment() {

    @Inject
    lateinit var shoppingDetailVM: ShoppingDetailVM

    private lateinit var goodTypeAdapter: GoodTypeAdapter
    private val args: GoodEditorDialogArgs by navArgs()
    private var isDone: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dlg_good_editor, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
        clickListeners()
        observeData()
    }

    private fun setup() {
        dlg_header.text = getString(
            if (args.goodId != null)
                R.string.dlg_edit_good_mode_edit
            else
                R.string.dlg_edit_good_mode_create
        )

        goodTypeAdapter = GoodTypeAdapter()
        dlg_good_selector_content.layoutManager = GridLayoutManager(requireContext(), 5)
        dlg_good_selector_content.adapter = goodTypeAdapter

        goodTypeAdapter.setList(List(GoodType.values().size) {
            GoodType.values()[it] toSelectionPair (it == 0)
        }.toMutableList())

        material_number_picker.minValue = 1
        material_number_picker.maxValue = 100

        dlg_input_field.requestFocus()
    }

    private fun clickListeners() {
        dlg_cancel.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribe {
                dismiss()
            }
            .addToSubscriptions()

        dlg_submit.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribe {
                actionSubmit()
                dismiss()
            }
            .addToSubscriptions()

        dlg_input_field.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                actionSubmit()
                dismiss()
            }
            false
        }
    }

    private fun actionSubmit() {
        if (args.goodId != null)
            shoppingDetailVM.update(
                Good(
                    id = args.goodId.requireNonNull(),
                    groupId = args.shoppingListId,
                    number = material_number_picker.value,
                    isDone = isDone,
                    name = dlg_input_field.text.toString(),
                    type = goodTypeAdapter.selection.requireNonNull()
                )
            )
        else
            goodTypeAdapter.selection?.let { selection ->
                shoppingDetailVM.insert(
                    Good(
                        id = UUID.randomUUID().toString(),
                        groupId = args.shoppingListId,
                        number = material_number_picker.value,
                        isDone = false,
                        name = dlg_input_field.text.toString(),
                        type = selection
                    )
                )
            }

    }

    private fun observeData() {
        args.goodId?.let {
            shoppingDetailVM.loadGood(it)
                .observe(viewLifecycleOwner, androidx.lifecycle.Observer { good ->
                    isDone = good.isDone
                    dlg_input_field.setText(good.name)
                    goodTypeAdapter.select(good.type)
                    material_number_picker.value = good.number
                })
        }
    }
}
