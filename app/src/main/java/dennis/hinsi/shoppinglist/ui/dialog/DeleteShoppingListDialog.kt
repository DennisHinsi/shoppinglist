/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.base.BaseSheetDialogFragment
import dennis.hinsi.shoppinglist.ui.RxAttrs
import dennis.hinsi.shoppinglist.ui.vm.ShoppingListEditorVM
import kotlinx.android.synthetic.main.dlg_delete_shopping_list.*
import kotlinx.android.synthetic.main.dlg_header.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DeleteShoppingListDialog : BaseSheetDialogFragment() {

    @Inject
    lateinit var shoppingListEditorVM: ShoppingListEditorVM

    private val args: DeleteShoppingListDialogArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dlg_delete_shopping_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        observeInput()
    }

    private fun observeData() {
        shoppingListEditorVM.load(args.shoppingListId).observe(viewLifecycleOwner, Observer {
            dlg_header.text = getString(R.string.dlg_delete_shopping_list_header, "\"" + it.name + "\"")
        })
    }

    private fun observeInput() {
        dlg_cancel.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribe { dismiss() }
            .addToSubscriptions()

        dlg_delete.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribe {
                shoppingListEditorVM.delete(args.shoppingListId)
                dismiss()
            }
            .addToSubscriptions()
    }
}