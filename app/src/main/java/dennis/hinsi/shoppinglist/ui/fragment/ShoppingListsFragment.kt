/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.fragment

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.SettingsActivity
import dennis.hinsi.shoppinglist.base.BaseFragment
import dennis.hinsi.shoppinglist.ui.RxAttrs
import dennis.hinsi.shoppinglist.ui.callback.ShoppingListAdapterCallback
import dennis.hinsi.shoppinglist.ui.adapter.ShoppingListsAdapter
import dennis.hinsi.shoppinglist.ui.requireNonNull
import dennis.hinsi.shoppinglist.ui.setupWithRecyclerView
import dennis.hinsi.shoppinglist.ui.vm.ShoppingListsVM
import kotlinx.android.synthetic.main.frg_shopping_lists.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class ShoppingListsFragment : BaseFragment() {

    @Inject
    lateinit var shoppingListsVM: ShoppingListsVM

    private lateinit var shoppingListsAdapter: ShoppingListsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_shopping_lists, container, false)

    override fun prepareUI() {
        shoppingListsAdapter = ShoppingListsAdapter()
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        recycler_view.adapter = shoppingListsAdapter
        setupAdapterCallback()
        fab_add.setupWithRecyclerView(recycler_view)
        setHasOptionsMenu(true)
    }

    override fun prepareOnClickListeners() {
        shoppingListsAdapter.clickStream
            .doOnError(::streamError)
            .subscribe { openDetailFragment(it.value) }
            .addToSubscriptions()

        fab_add.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .doOnError(::streamError)
            .subscribe { swipeEditAction(id = null) }
            .addToSubscriptions()
    }

    override fun observeData() {
        shoppingListsVM.observe().observe(viewLifecycleOwner, Observer { shoppingList ->
            shoppingListsAdapter.list = shoppingList
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_shopping_lists, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_settings) {
            startActivity(Intent(requireContext(), SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupAdapterCallback() {
        ItemTouchHelper(ShoppingListAdapterCallback(
            iconRightToLeft = ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_delete_24,
                requireContext().theme
            ).requireNonNull(),

            iconLeftToRight = ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_edit_24,
                requireContext().theme
            ).requireNonNull(),

            backgroundRightToLeft = ColorDrawable(Color.RED),

            backgroundLeftToRight = ColorDrawable(Color.GRAY),

            adapter = shoppingListsAdapter
        ) { direction, id ->
            if (direction == ItemTouchHelper.RIGHT)
                swipeDeleteAction(id)
            else
                swipeEditAction(id)
        }).also { helper -> helper.attachToRecyclerView(recycler_view) }
    }

    private fun swipeEditAction(id: String?) {
        findNavController().navigate(
            ShoppingListsFragmentDirections.actionShoppingListsFragmentToShoppingListEditorDialog(
                shoppingListId = id
            )
        )
    }

    private fun swipeDeleteAction(id: String) {
        findNavController().navigate(
            ShoppingListsFragmentDirections.actionShoppingListsFragmentToDeleteShoppingListDialog(
                shoppingListId = id
            )
        )
    }

    private fun openDetailFragment(id: String) {
        findNavController().navigate(
            ShoppingListsFragmentDirections.actionShoppingListsFragmentToShoppingDetailFragment(
                shoppingListId = id
            )
        )
    }
}