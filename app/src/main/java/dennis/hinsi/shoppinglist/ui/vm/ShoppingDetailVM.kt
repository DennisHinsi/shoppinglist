/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.vm

import androidx.lifecycle.liveData
import dennis.hinsi.shoppinglist.base.CoroutineViewModel
import dennis.hinsi.domain.model.Good
import dennis.hinsi.domain.usecases.GoodReaderUC
import dennis.hinsi.domain.usecases.GoodWriterUC
import dennis.hinsi.domain.usecases.ShoppingListReaderUC
import kotlinx.coroutines.launch
import javax.inject.Inject

class ShoppingDetailVM @Inject constructor(
    private val shoppingListReaderUC: ShoppingListReaderUC,
    private val goodReaderUC: GoodReaderUC,
    private val goodWriterUC: GoodWriterUC
) : CoroutineViewModel() {

    fun load(id: String) = shoppingListReaderUC.load(id)

    fun updateDoneState(id: String) = backgroundScope.launch(coroutineExceptions) {
        update(goodReaderUC.load(id).apply {
            isDone = !isDone
        })
    }

    fun updateGroupDoneState(
        groupId: String,
        newState: Boolean
    ) = backgroundScope.launch(coroutineExceptions) {
        goodReaderUC.loadGroup(groupId).forEach { good ->
            update(good.apply { isDone = newState })
        }
    }

    fun observerGroup(groupId: String) = goodReaderUC.observeGroup(groupId)

    fun loadGood(id: String) = liveData {
        emit(goodReaderUC.load(id))
    }

    fun insert(good: Good) = backgroundScope.launch(coroutineExceptions) {
        goodWriterUC.insert(good)
    }

    fun update(good: Good) = backgroundScope.launch(coroutineExceptions) {
        goodWriterUC.update(good)
    }

    fun delete(goodId: String) = backgroundScope.launch(coroutineExceptions) {
        goodWriterUC.delete(goodId)
    }
}