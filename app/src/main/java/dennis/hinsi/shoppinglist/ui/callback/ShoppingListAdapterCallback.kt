/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.callback

import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import dennis.hinsi.shoppinglist.ui.adapter.ShoppingListsAdapter


class ShoppingListAdapterCallback(
    private var iconRightToLeft: Drawable,
    private var iconLeftToRight: Drawable,
    private var backgroundRightToLeft: ColorDrawable,
    private var backgroundLeftToRight: ColorDrawable,
    private val adapter: ShoppingListsAdapter,
    private val onSwiped: (Int, String) -> Unit
) : ItemTouchHelper.SimpleCallback(
    0,
    ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
) {

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        onSwiped(direction, adapter.list[position].id)
        adapter.notifyItemChanged(position)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean = true

    override fun onChildDraw(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        state: Int,
        isActive: Boolean
    ) {
        val itemRootView = viewHolder.itemView

        when {
            dX < 0 -> {
                val iconMargin = (itemRootView.height - iconLeftToRight.intrinsicHeight) / 2
                val iconTop = itemRootView.top + iconMargin
                val iconBottom = iconTop + iconLeftToRight.intrinsicHeight
                val iconLeft = itemRootView.right - iconMargin - iconLeftToRight.intrinsicWidth
                val iconRight = itemRootView.right - iconMargin

                iconLeftToRight.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                backgroundLeftToRight.draw(canvas)
                iconLeftToRight.draw(canvas)

                val swipeDrawBorder = -(itemRootView.width - itemRootView.height * 2)
                val leftBorder = -(itemRootView.width + dX)
                if (leftBorder < swipeDrawBorder) {
                    backgroundLeftToRight.setBounds(
                        itemRootView.right + dX.toInt(),
                        itemRootView.top, itemRootView.right, itemRootView.bottom
                    )
                    super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, state, isActive)
                }
            }
            dX > 0 -> {
                val iconMargin = (itemRootView.height - iconRightToLeft.intrinsicHeight) / 2
                val iconTop = itemRootView.top + iconMargin
                val iconBottom = iconTop + iconRightToLeft.intrinsicHeight
                val iconLeft = itemRootView.left + iconMargin
                val iconRight = itemRootView.left + iconMargin + iconRightToLeft.intrinsicWidth

                iconRightToLeft.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                backgroundRightToLeft.draw(canvas)
                iconRightToLeft.draw(canvas)

                val swipeDrawBorder = itemRootView.width + itemRootView.height * 2
                val leftBorder = itemRootView.width + dX
                if (leftBorder < swipeDrawBorder) {
                    backgroundRightToLeft.setBounds(
                        itemRootView.left + dX.toInt(),
                        itemRootView.top, itemRootView.left, itemRootView.bottom
                    )
                    super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, state, isActive)
                }
            }
        }
    }
}

