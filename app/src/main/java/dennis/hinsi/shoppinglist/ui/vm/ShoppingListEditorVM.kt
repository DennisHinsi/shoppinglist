/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.vm

import dennis.hinsi.shoppinglist.base.CoroutineViewModel
import dennis.hinsi.domain.model.ShoppingList
import dennis.hinsi.domain.usecases.ShoppingListReaderUC
import dennis.hinsi.domain.usecases.ShoppingListWriterUC
import kotlinx.coroutines.launch
import javax.inject.Inject

class ShoppingListEditorVM @Inject constructor(
    private val shoppingListReaderUC: ShoppingListReaderUC,
    private val shoppingListWriterUC: ShoppingListWriterUC
) : CoroutineViewModel() {

    fun insert(shoppingList: ShoppingList) = backgroundScope.launch(coroutineExceptions) {
        shoppingListWriterUC.insert(shoppingList)
    }

    fun update(shoppingList: ShoppingList) = backgroundScope.launch(coroutineExceptions) {
        shoppingListWriterUC.update(shoppingList)
    }

    fun load(id: String) = shoppingListReaderUC.load(id)

    fun delete(id: String) = backgroundScope.launch(coroutineExceptions) {
        shoppingListWriterUC.delete(id)
    }

}