/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.afterTextChangeEvents
import dennis.hinsi.shoppinglist.R
import dennis.hinsi.shoppinglist.base.BaseSheetDialogFragment
import dennis.hinsi.domain.model.ShoppingList
import dennis.hinsi.shoppinglist.ui.RxAttrs
import dennis.hinsi.shoppinglist.ui.vm.ShoppingListEditorVM
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.dlg_header.*
import kotlinx.android.synthetic.main.dlg_shopping_list_editor.*
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ShoppingListEditorDialog : BaseSheetDialogFragment() {

    @Inject
    lateinit var shoppingListEditorVM: ShoppingListEditorVM

    private val args: ShoppingListEditorDialogArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dlg_shopping_list_editor, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        observeInput()
        loadData()
    }

    private fun setupUI() {
        dlg_header.text = getString(
            if (args.shoppingListId != null)
                R.string.dlg_shopping_list_editor_header_mode_edit
            else
                R.string.dlg_shopping_list_editor_header_mode_create
        )
    }

    private fun observeInput() {
        dlg_input_field.afterTextChangeEvents()
            .skipInitialValue()
            .distinctUntilChanged()
            .debounce(RxAttrs.TEXT_CHANGES_TIMEOUT, TimeUnit.MILLISECONDS)
            .doOnError(::streamError)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { dlg_submit.isEnabled = it.editable?.isNotEmpty() == true }
            .addToSubscriptions()

        dlg_cancel.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribe { dismiss() }
            .addToSubscriptions()

        dlg_submit.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribe { actionSubmit() }
            .addToSubscriptions()
    }

    private fun actionSubmit() {
        val currentId = args.shoppingListId

        if (currentId == null)
            shoppingListEditorVM.insert(
                ShoppingList(
                    id = UUID.randomUUID().toString(),
                    name = dlg_input_field.text.toString(),
                    modified = System.currentTimeMillis()
                )
            )
        else
            shoppingListEditorVM.update(
                ShoppingList(
                    id = currentId,
                    name = dlg_input_field.text.toString(),
                    modified = System.currentTimeMillis()
                )
            )

        dismiss()
    }

    private fun loadData() {
        args.shoppingListId?.let { id ->
            shoppingListEditorVM.load(id).observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                dlg_input_field.setText(it.name)
            })
        }
    }
}