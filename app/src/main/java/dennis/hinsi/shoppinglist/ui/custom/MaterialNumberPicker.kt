/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.ui.custom

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import dennis.hinsi.shoppinglist.R
import kotlinx.android.synthetic.main.custom_number_picker.view.*

class MaterialNumberPicker @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var value = DEFAULT_MIN_VALUE
        set(value) {
            field = value
            number_picker_text.text = "$value"
        }

    var minValue = DEFAULT_MIN_VALUE
        set(value) {
            if (value < maxValue)
                field = value
            else
                throw IndexOutOfBoundsException()
        }

    var maxValue = DEFAULT_MAX_VALUE
        set(value) {
            if (value > minValue)
                field = value
            else
                throw IndexOutOfBoundsException()
        }

    init {
        inflate(context, R.layout.custom_number_picker, this)
        number_picker_text.text = "$minValue"
        onClickListeners()
    }


    private fun onClickListeners() {
        number_picker_minus.setOnClickListener {
            value = number_picker_text.text.toString().toInt()
            if (value > minValue) {
                value--
                number_picker_text.text = "$value"
            }
        }

        number_picker_plus.setOnClickListener {
            if (value < maxValue) {
                value = number_picker_text.text.toString().toInt()
                value++
                number_picker_text.text = "$value"
            }
        }
    }

    private companion object {
        const val DEFAULT_MIN_VALUE = 1
        const val DEFAULT_MAX_VALUE = 10
    }
}