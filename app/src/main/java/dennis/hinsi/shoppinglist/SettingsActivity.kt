/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import com.jakewharton.rxbinding4.view.clicks
import dennis.hinsi.domain.model.SettingsAbout
import dennis.hinsi.shoppinglist.ui.RxAttrs
import dennis.hinsi.shoppinglist.ui.adapter.SettingsAboutAdapter
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_settings.*
import java.util.concurrent.TimeUnit

class SettingsActivity : AppCompatActivity() {

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setup()
        onClickListeners()
    }

    private fun setup() {
        settings_content.layoutManager = LinearLayoutManager(this)
        settings_content.adapter = SettingsAboutAdapter().apply {
            list = listOf(
                SettingsAbout(
                    titleKey = getString(R.string.settings_about_author_key),
                    titleValue = getString(R.string.settings_about_author_value)
                ),
                SettingsAbout(
                    titleKey = getString(R.string.settings_about_source_code_key),
                    titleValue = getString(R.string.settings_about_source_code_value)
                ),
                SettingsAbout(
                    titleKey = getString(R.string.settings_about_version_key),
                    titleValue = BuildConfig.VERSION_NAME
                ),
                SettingsAbout(
                    titleKey = getString(R.string.settings_about_build_key),
                    titleValue = BuildConfig.BUILD_TYPE
                )
            )
        }
    }

    private fun onClickListeners() {
        settings_licenses.clicks()
            .debounce(RxAttrs.CLICK_TIMEOUT, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                OssLicensesMenuActivity.setActivityTitle(getString(R.string.settings_licenses))
                startActivity(Intent(this, OssLicensesMenuActivity::class.java))
            }
            .also { disposables.add(it) }
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

}