/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.dagger

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dennis.hinsi.shoppinglist.CustomApplication
import dennis.hinsi.shoppinglist.dagger.modules.contributors.ContributeFragmentModule
import dennis.hinsi.shoppinglist.dagger.modules.contributors.ContributeViewModelModule
import dennis.hinsi.shoppinglist.dagger.modules.AppModule
import dennis.hinsi.shoppinglist.dagger.modules.RepositoryModule
import dennis.hinsi.shoppinglist.dagger.modules.RoomModule
import dennis.hinsi.shoppinglist.dagger.modules.UCModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        RoomModule::class,
        RepositoryModule::class,
        UCModule::class,
        ContributeFragmentModule::class,
        ContributeViewModelModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun appModule(appModule: AppModule): Builder
        fun roomModule(roomModule: RoomModule): Builder
        fun ucModule(ucModule: UCModule): Builder
        fun repositoryModule(repositoryModule: RepositoryModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: CustomApplication)
}
