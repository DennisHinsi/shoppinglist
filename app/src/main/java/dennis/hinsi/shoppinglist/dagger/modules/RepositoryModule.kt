/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.dagger.modules

import dagger.Module
import dagger.Provides
import dennis.hinsi.data.database.GoodDAO
import dennis.hinsi.data.database.ShoppingListDAO
import dennis.hinsi.data.repository.GoodRepositoryImpl
import dennis.hinsi.data.repository.ShoppingListListRepositoryImpl
import dennis.hinsi.domain.repository.GoodRepository
import dennis.hinsi.domain.repository.ShoppingListRepository
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideGoodRepository(goodDAO: GoodDAO): GoodRepository =
        GoodRepositoryImpl(goodDAO)

    @Singleton
    @Provides
    fun provideShoppingListRepository(
        shoppingListDAO: ShoppingListDAO
    ): ShoppingListRepository =
        ShoppingListListRepositoryImpl(
            shoppingListDAO
        )
}