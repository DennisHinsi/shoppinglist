/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.dagger.modules.contributors

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dennis.hinsi.shoppinglist.dagger.vm.ViewModelFactory
import dennis.hinsi.shoppinglist.dagger.vm.ViewModelKey
import dennis.hinsi.shoppinglist.ui.vm.ShoppingDetailVM
import dennis.hinsi.shoppinglist.ui.vm.ShoppingListEditorVM
import dennis.hinsi.shoppinglist.ui.vm.ShoppingListsVM

@Module
abstract class ContributeViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ShoppingListsVM::class)
    internal abstract fun provideShoppingListsVM(viewModel: ShoppingListsVM): ViewModel
    
    @Binds
    @IntoMap
    @ViewModelKey(ShoppingListEditorVM::class)
    internal abstract fun provideShoppingListEditorVM(viewModel: ShoppingListEditorVM): ViewModel
    
    @Binds
    @IntoMap
    @ViewModelKey(ShoppingDetailVM::class)
    internal abstract fun provideShoppingDetailVM(viewModel: ShoppingDetailVM): ViewModel
}
