/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.dagger.modules

import dagger.Module
import dagger.Provides
import dennis.hinsi.domain.repository.GoodRepository
import dennis.hinsi.domain.repository.ShoppingListRepository
import dennis.hinsi.domain.usecases.GoodReaderUC
import dennis.hinsi.domain.usecases.GoodWriterUC
import dennis.hinsi.domain.usecases.ShoppingListReaderUC
import dennis.hinsi.domain.usecases.ShoppingListWriterUC
import javax.inject.Singleton

@Module
class UCModule {

    @Provides
    @Singleton
    fun provideShoppingListReaderUC(
        shoppingListRepository: ShoppingListRepository
    ): ShoppingListReaderUC = ShoppingListReaderUC(shoppingListRepository)

    @Provides
    @Singleton
    fun provideShoppingListWriterUC(
        shoppingListRepository: ShoppingListRepository
    ): ShoppingListWriterUC = ShoppingListWriterUC(shoppingListRepository)

    @Provides
    @Singleton
    fun provideGoodReaderUC(
        goodRepository: GoodRepository
    ): GoodReaderUC = GoodReaderUC(goodRepository)

    @Provides
    @Singleton
    fun provideGoodWriterUC(
        goodRepository: GoodRepository,
        shoppingListWriterUC: ShoppingListWriterUC
    ): GoodWriterUC = GoodWriterUC(goodRepository, shoppingListWriterUC)

}