/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist.dagger.modules.contributors

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dennis.hinsi.shoppinglist.dagger.FragmentScope
import dennis.hinsi.shoppinglist.ui.dialog.DeleteGoodDialog
import dennis.hinsi.shoppinglist.ui.dialog.DeleteShoppingListDialog
import dennis.hinsi.shoppinglist.ui.dialog.GoodEditorDialog
import dennis.hinsi.shoppinglist.ui.dialog.ShoppingListEditorDialog
import dennis.hinsi.shoppinglist.ui.fragment.ShoppingDetailFragment
import dennis.hinsi.shoppinglist.ui.fragment.ShoppingListsFragment


@Module
abstract class ContributeFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeShoppingListsFragment(): ShoppingListsFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeShoppingDetailFragment(): ShoppingDetailFragment?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeDeleteShoppingListDialog(): DeleteShoppingListDialog?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeShoppingListEditorDialog(): ShoppingListEditorDialog?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeGoodEditorDialog(): GoodEditorDialog?

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun contributeDeleteGoodDialog(): DeleteGoodDialog?

}