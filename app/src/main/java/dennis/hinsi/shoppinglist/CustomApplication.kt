/*
 * Copyright 2020 dennis hinsi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dennis.hinsi.shoppinglist

import android.app.Application
import androidx.emoji.bundled.BundledEmojiCompatConfig
import androidx.emoji.text.EmojiCompat
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import dennis.hinsi.shoppinglist.dagger.modules.AppModule
import dennis.hinsi.shoppinglist.dagger.DaggerAppComponent
import dennis.hinsi.shoppinglist.dagger.modules.RepositoryModule
import dennis.hinsi.shoppinglist.dagger.modules.RoomModule
import dennis.hinsi.shoppinglist.dagger.modules.UCModule
import javax.inject.Inject

class CustomApplication : Application() , HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .application(this)
            .appModule(AppModule(this))
            .roomModule(RoomModule(this))
            .repositoryModule(RepositoryModule())
            .ucModule(UCModule())
            .build()
            .inject(this)

        EmojiCompat.init(BundledEmojiCompatConfig(this))
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

}